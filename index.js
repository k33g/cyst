/**
  @license MIT
  Copyright (c) 2020 Philippe Charrière
  Twitter: @k33g_org
  GitLab: @k33g
*/

export const cyst = {
  items: Object.entries(localStorage),
  getItem: (key) => localStorage.getItem(key),
  setItem: (key, value) => localStorage.setItem(key, value)
}
