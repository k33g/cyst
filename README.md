# Cyst 🍳

**Cyst** is a helper to ease the use of the JavaScript `localStorage` object of the browser

## Usage



```html
<script type="module">
  import { cyst } from './index.js'
  console.log(cyst)
  cyst.setItem("german_salutation", "Morgen")
  cyst.setItem("english_salutation", "Hello")
  cyst.setItem("french_salutation", "Salut")

  console.log(
    cyst.items.filter(item => item[0].includes("salutation")) // 3 records
  )
  console.log(
    cyst.items.find(item => item[0].includes("salutation")) // 1 record ["german_salutation", "Morgen"]
  )
  console.log(
    cyst.items.forEach(item => console.log("key:", item[0], "value:", item[1]))
  )

</script>
```

## Test it

```bash
npx http-server
```

Then open [http://localhost:8080/](http://localhost:8080/) and look at the console of your browser

## Install it

- Registry Setup: `echo @k33g:registry=https://gitlab.com/api/v4/packages/npm >> .npmrc`
- Installation: `npm i @k33g/cyst`

